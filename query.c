#include "graph.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int query_prefix(graphDB *db,char *prefix){
    char key[33];
    md5(prefix,key);
    int hv = hash33(key);
    hv = hv % db->prefix_edgeDB.maxHash;
    int tid = db->prefix_edgeDB.hashTab[hv];
    while(tid != 0){
        if(tid>0){
            if(!strcmp(db->prefix_edgeDB.edges[tid].id,key)){
                return tid;
            }
        }
        hv = (hv+1) % db->termDB.maxHash;
        tid = db->termDB.hashTab[hv]; 
    }
    return 0;
}

int query_terms_by_sound(graphDB *db,char *sound){
    char key[33];
    md5(sound,key);
    int hv = hash33(key);
    hv = hv % db->full_sound_edgeDB.maxHash;
    int tid = db->full_sound_edgeDB.hashTab[hv];
    while(tid != 0){
        if(tid>0){
            if(!strcmp(db->full_sound_edgeDB.edges[tid].id,key)){
                return tid;
            }
        }
        hv = (hv+1) % db->full_sound_edgeDB.maxHash;
        tid = db->full_sound_edgeDB.hashTab[hv]; 
    }
    return 0;
}

int query_term(graphDB *db,char *term){
    char key[33];
    md5(term,key);
    int hv = hash33(key);
    hv = hv % db->termDB.maxHash;
    int tid = db->termDB.hashTab[hv];
    while(tid != 0){
        if(tid>0){
            if(!strcmp(db->termDB.nodes[tid].term,term)){
                return tid;
            }
        }
        hv = (hv+1) % db->termDB.maxHash;
        tid = db->termDB.hashTab[hv]; 
    }
    return 0;   
}

int query_term_edge(graphDB *db,char *term1,char *term2){
    char key1[33],key2[33],concat_key[65],edge_key[33];
    printf("%s\n",term1);
    printf("%s\n",term2);
    memset(key1,'\0',sizeof(char)*33);
    memset(key2,'\0',sizeof(char)*33);
    memset(concat_key,'\0',sizeof(char)*65);
    memset(edge_key,'\0',sizeof(char)*33);
    md5(term1,key1);
    md5(term2,key2);
    strcat(concat_key,key1);
    strcat(concat_key,key2);
    md5(concat_key,edge_key);
    int hv = hash33(edge_key);
    hv = hv % db->term_edgeDB.maxHash;
    int tid = db->term_edgeDB.hashTab[hv];
    while(tid !=0 ){
        if(tid>0){
            if(!strcmp(db->term_edgeDB.edges[tid].id,edge_key)){
                return tid;
            }
        }
        hv = (hv+1) % db->term_edgeDB.maxHash;
        tid = db->term_edgeDB.hashTab[hv]; 
    }
    return 0;   
}