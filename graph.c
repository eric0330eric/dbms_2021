#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include "graph.h"

int get_current_time_stamp(){
    return (int)time(NULL);
}

unsigned long long hash33(unsigned char *key){
	unsigned char *ptr = key;
	unsigned long long hashv;

	hashv = 0;
	while (*ptr){
		hashv = (hashv << 5) + hashv + *ptr++;
	}
	return hashv;
}

void md5(char *word, char *hash_code){
    memset(hash_code,'\0',sizeof(33*sizeof(char)));
	MD5_CTX md5_context;
	MD5_Init(&md5_context);
	unsigned char decrypt[17];							  //存放加密後的結果
	MD5_Update(&md5_context, word, strlen((char *)word)); //對欲加密的字符進行加密
	MD5_Final(decrypt, &md5_context);
	char *ktr = hash_code;
	for (int i = 0; i < 16; i++)
	{
		sprintf(ktr, "%02x", decrypt[i]);
		ktr += 2;
	}
	ktr = '\0';
}

void graph_init(graphDB *db){
    term_db_init(&db->termDB);
    sound_db_init(&db->soundDB);
    full_sound_edge_db_init(&db->full_sound_edgeDB);    
    prefix_edge_db_init(&db->prefix_edgeDB);
    term_edge_db_init(&db->term_edgeDB);
}

void creat_term_node(term_node *new_node,char *term){
    strcpy(new_node->term,term);
    new_node->total_freq = 1;
    new_node->last_use = get_current_time_stamp();
    for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
        new_node->prev_edge[i] = 0;
        new_node->next_edge[i] = 0;
        // memset(new_node->prev_edge[i],'\0',sizeof(char)*ID_LENGTH);
        // memset(new_node->next_edge[i],'\0',sizeof(char)*ID_LENGTH);
    }
    // new_node->full_cnt = 0;
    for(int i=0;i<SOUND_ARRAY_LENGTH;i++){
        new_node->full_sound[i] = 0;
        // memset(new_node->full_sound[i],'\0',sizeof(char)*ID_LENGTH);
    }
}

void term_db_init(term_db *db){
    db->term_cnt = 3;
    db->total_cnt = 3;
    db->maxHash = MAX_TERM_CNT * 2 + 1;
    db->last_term_idx = 3;
    creat_term_node(&db->nodes[0],"dummyterm\0");
    creat_term_node(&db->nodes[1],"<begin>\0");
    creat_term_node(&db->nodes[2],"<end>\0");
    memset(db->hashTab,0,sizeof(int)*db->maxHash);
}

void creat_sound_node(sound_node *new_node,char *sound){
    strcpy(new_node->sound,sound);
}

void sound_db_init(sound_db *db){
    db->total_cnt = 1;
    db->maxHash = SOUND_DB_LENGTH*2+1;
    memset(db->hashTab,0,sizeof(int)*db->maxHash);
    creat_sound_node(&db->nodes[0],"dummySound\0");
}

void creat_full_sound_edge(full_sound_edge *new_node,char *sound){
    md5(sound,new_node->id);
    for(int i=0;i<SOUND_LENGTH;i++){
        new_node->sound_side[i] = 0;
    }

    for(int i=0;i<TERM_ARRAY_LENGTH;i++){
        new_node->term_side[i] = 0;
        new_node->freq[i] = 0;
    }
}

void full_sound_edge_db_init(full_sound_edge_db *db){
    db->total_cnt = 1;
    db->maxHash = MAX_EDGE_CNT*2+1;
    memset(db->hashTab,0,sizeof(int)*db->maxHash);
    creat_full_sound_edge(&db->edges[0],"dummyFullSound\0");
}

void creat_prefix_edge(prefix_edge *new_node,char *sound){
    md5(sound,new_node->id);
    for(int i=0;i<TERM_ARRAY_LENGTH;i++){
        new_node->term_side[i] = 0;
        new_node->freq[i] = 0;
    }
}

void prefix_edge_db_init(prefix_edge_db *db){
    db->total_cnt = 1;
    db->maxHash = MAX_EDGE_CNT*2+1;
    memset(db->hashTab,0,sizeof(int)*db->maxHash);
    creat_prefix_edge(&db->edges[0],"dummyPrefixSound\0");
}

void creat_term_edge(term_edge *new_node,char* key1, char* key2,int tid1,int tid2){
    char concat_key[65];
    memset(concat_key,'\0',sizeof(char)*65);
    strcat(concat_key,key1);
    strcat(concat_key,key2);
    md5(concat_key,new_node->id);
    new_node->point_from = tid1;
    new_node->point_to = tid2;
    new_node->freq = 1;
    new_node->last_use = get_current_time_stamp();
}

void term_edge_db_init(term_edge_db *db){
    db->maxHash = MAX_EDGE_CNT*2+1;
    db->total_cnt = 1;
    memset(db->hashTab,0,sizeof(int)*db->maxHash);
    // d624199ebbc1fadd0a0a49dcbedc1248 : dummyterm
    creat_term_edge(&db->edges[0],"d624199ebbc1fadd0a0a49dcbedc1248\0","d624199ebbc1fadd0a0a49dcbedc1248\0",0,0);
}

int sent_insert(graphDB *db, char *sent, char *sound){   
    char *ptr = sent;
    char *sound_ptr = sound;
    char term[1024];
    char current_sound[128];
    char current_sound_key[33],prefix_key[33],front_key[33],end_key[33],edge_key[33];
    char concat_key[65];
    int front_term_idx = 1;
    int end_term_idx = 0;
    term_node *front_term = &db->termDB.nodes[1];
    term_node *end_term = NULL;
    int hv,tid;
    if(strlen(sent) && strlen(sound)){
        while(front_term_idx != 2){
            int first_del_hv= -1;
            if(end_term_idx != 2){
                memset(term,'\0',1024*sizeof(char));
                sscanf(ptr,"%s",term);
                ptr += strlen(term) + 1;
                size_t term_len = 1;
                if(!isalnum(term[0])){
                    term_len = strlen(term)/3;
                }
                db->termDB.total_cnt += 1;
                // 新增詞node
                md5(term,end_key);
                hv = hash33(end_key);
                hv = hv % db->termDB.maxHash;
                tid = db->termDB.hashTab[hv];
                first_del_hv= -1;
                while(tid != 0){
                    if(tid < 0){
                        if(first_del_hv < 0){
                            first_del_hv = hv;
                        }
                    }
                    else if(!strcmp(db->termDB.nodes[tid].term,term)){
                        //handle sound frequency
                        end_term_idx = tid;
                        end_term = &db->termDB.nodes[tid];
                        db->termDB.nodes[tid].total_freq += 1;
                        db->termDB.nodes[tid].last_use = get_current_time_stamp();
                        break;
                    }
                    hv = (hv+1) % db->termDB.maxHash;
                    tid = db->termDB.hashTab[hv]; 
                } 
                if(tid == 0){
                    int new_idx = db->termDB.last_term_idx;
                    int new_hv = hv;
                    if(first_del_hv >= 0){
                        new_hv = first_del_hv;
                        new_idx = db->termDB.hashTab[first_del_hv] * -1;
                    }
                    creat_term_node(&db->termDB.nodes[new_idx],term); 
                    end_term = &db->termDB.nodes[new_idx];
                    db->termDB.hashTab[new_hv] = new_idx;
                    end_term_idx = new_idx;
                    db->termDB.term_cnt+=1;
                    if(first_del_hv<0){
                        db->termDB.last_term_idx+=1;
                    }
                }
                // 新增單一聲音節點
                memset(current_sound,'\0',sizeof(char)*128);
                int node_id_arr[term_len];
                int prefix_len = (term_len+1)/2;
                int prefix_cut = 0;
                for(int i=0;i<term_len;i++){
                    if(i == prefix_len){
                        prefix_cut = strlen(current_sound);
                    }
                    strcat(current_sound,sound_ptr);
                    md5(sound_ptr,current_sound_key);
                    hv = hash33(current_sound_key);
                    hv = hv % db->soundDB.maxHash;
                    tid = db->soundDB.hashTab[hv];
                    while(tid>0){
                        if(!strcmp(db->soundDB.nodes[tid].sound,sound_ptr)){
                            //handle sound frequency
                            node_id_arr[i] = tid;
                            break;
                        }
                        hv = (hv+1) % db->soundDB.maxHash;
                        tid = db->soundDB.hashTab[hv]; 
                    }
                    if(tid == 0){
                        creat_sound_node(&db->soundDB.nodes[db->soundDB.total_cnt],sound_ptr);
                        node_id_arr[i] = db->soundDB.total_cnt;
                        db->soundDB.hashTab[hv] = db->soundDB.total_cnt;
                        db->soundDB.total_cnt+=1;
                    }
                    sound_ptr += strlen(sound_ptr) + 1;
                }
                //新增full sound edge
                md5(current_sound,current_sound_key);
                hv = hash33(current_sound_key);
                hv = hv % db->full_sound_edgeDB.maxHash;
                tid = db->full_sound_edgeDB.hashTab[hv];
                first_del_hv = -1;
                while(tid != 0){
                    if(tid < 0){
                        if(first_del_hv < 0){
                            first_del_hv = hv;
                        }
                    }
                    else if(!strcmp(db->full_sound_edgeDB.edges[tid].id,current_sound_key)){
                        //handle
                        int flag = 0;
                        for(int i=0;i<TERM_ARRAY_LENGTH;i++){
                            if(db->full_sound_edgeDB.edges[tid].term_side[i]==end_term_idx){
                                db->full_sound_edgeDB.edges[tid].freq[i]+=1;
                                flag = 1;
                                break;
                            }
                        }
                        if(!flag){
                            for(int i=0;i<TERM_ARRAY_LENGTH;i++){
                                if(db->full_sound_edgeDB.edges[tid].term_side[i]==0){
                                    db->full_sound_edgeDB.edges[tid].term_side[i] = end_term_idx;
                                    db->full_sound_edgeDB.edges[tid].freq[i] = 1;
                                    // 對詞節點中的全音陣列 新增此讀音
                                    for(int j=0;j<SOUND_ARRAY_LENGTH;j++){
                                        if(end_term->full_sound[j] == 0){
                                            end_term->full_sound[j] = tid;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    hv = (hv+1) % db->full_sound_edgeDB.maxHash;
                    tid = db->full_sound_edgeDB.hashTab[hv]; 
                }
                if(tid == 0){
                    int new_idx = db->full_sound_edgeDB.total_cnt;
                    int new_hv = hv;
                    if(first_del_hv >= 0){
                        new_hv = first_del_hv;
                        new_idx = db->full_sound_edgeDB.hashTab[first_del_hv] * -1;
                    }
                    creat_full_sound_edge(&db->full_sound_edgeDB.edges[new_idx],current_sound);
                    db->full_sound_edgeDB.hashTab[hv] = new_idx;
                    for(int i=0;i<term_len;i++){
                        db->full_sound_edgeDB.edges[new_idx].sound_side[i] = node_id_arr[i];
                    }
                    db->full_sound_edgeDB.edges[new_idx].term_side[0] = end_term_idx;
                    db->full_sound_edgeDB.edges[new_idx].freq[0] = 1;
                    //對詞節點中的全音陣列 新增此讀音
                    for(int j=0;j<SOUND_ARRAY_LENGTH;j++){
                        if(end_term->full_sound[j] == 0){
                            end_term->full_sound[j] = new_idx;
                            break;
                        }
                    }
                    if(first_del_hv < 0){
                        db->full_sound_edgeDB.total_cnt += 1;
                    }
                }
                if(term_len > 1){
                    //新增prefix edge
                    current_sound[prefix_cut] = '\0';
                    md5(current_sound,prefix_key);
                    hv = hash33(prefix_key);
                    hv = hv % db->prefix_edgeDB.maxHash;
                    tid = db->prefix_edgeDB.hashTab[hv];
                    first_del_hv = -1;
                    while(tid != 0){
                        if(tid < 0){
                            if(first_del_hv < 0){
                                first_del_hv = hv;
                            }
                        }
                        else if(!strcmp(db->prefix_edgeDB.edges[tid].id,prefix_key)){
                            //handle
                            int flag = 0;
                            for(int i=0;i<TERM_ARRAY_LENGTH;i++){
                                if(db->prefix_edgeDB.edges[tid].term_side[i]==end_term_idx){
                                    db->prefix_edgeDB.edges[tid].freq[i]+=1;
                                    flag = 1;
                                    break;
                                }
                            }
                            if(!flag){
                                for(int i=0;i<TERM_ARRAY_LENGTH;i++){
                                    if(db->prefix_edgeDB.edges[tid].term_side[i]==0){
                                        db->prefix_edgeDB.edges[tid].term_side[i] = end_term_idx;
                                        db->prefix_edgeDB.edges[tid].freq[i] = 1;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                        hv = (hv+1) % db->prefix_edgeDB.maxHash;
                        tid = db->prefix_edgeDB.hashTab[hv]; 
                    }
                    if(tid == 0){
                        int new_idx = db->prefix_edgeDB.total_cnt;
                        int new_hv = hv;
                        if(first_del_hv >= 0){
                            new_hv = first_del_hv;
                            new_idx = db->prefix_edgeDB.hashTab[first_del_hv] * -1;
                        }
                        creat_prefix_edge(&db->prefix_edgeDB.edges[new_idx],current_sound);
                        db->prefix_edgeDB.hashTab[new_hv] = new_idx;
                        db->prefix_edgeDB.edges[new_idx].term_side[0] = end_term_idx;
                        db->prefix_edgeDB.edges[new_idx].freq[0] = 1;
                        if(first_del_hv < 0){
                            db->prefix_edgeDB.total_cnt += 1;
                        }
                    }
                }
            }
            //新增兩詞間連結
            md5(front_term->term,front_key);
            memset(concat_key,'\0',sizeof(char)*65);
            strcat(concat_key,front_key);
            strcat(concat_key,end_key);
            md5(concat_key,edge_key);
            hv = hash33(edge_key);
            hv = hv % db->term_edgeDB.maxHash;
            tid = db->term_edgeDB.hashTab[hv];
            first_del_hv= -1;
            while(tid != 0){
                if(tid < 0){
                    if(first_del_hv < 0){
                        first_del_hv = hv;
                    }
                }
                else if(!strcmp(db->term_edgeDB.edges[tid].id,edge_key)){
                    //handle
                    db->term_edgeDB.edges[tid].freq+=1;
                    db->term_edgeDB.edges[tid].last_use = get_current_time_stamp();
                    break;
                }
                hv = (hv+1) % db->term_edgeDB.maxHash;
                tid = db->term_edgeDB.hashTab[hv]; 
            }
            if(tid == 0){
                int new_idx = db->term_edgeDB.total_cnt;
                int new_hv = hv;
                if(first_del_hv >= 0){
                    new_hv = first_del_hv;
                    new_idx = db->term_edgeDB.hashTab[first_del_hv] * -1;
                }
                creat_term_edge(&db->term_edgeDB.edges[new_idx],front_key,end_key,front_term_idx,end_term_idx);
                for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
                    if(front_term->next_edge[i] == 0){
                        front_term->next_edge[i] = new_idx;
                        break;
                    }
                }
                for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
                    if(end_term->prev_edge[i] == 0){
                        end_term->prev_edge[i] = new_idx;
                        break;
                    }
                }
                db->term_edgeDB.hashTab[new_hv] = new_idx;
                if(first_del_hv<0){
                    db->term_edgeDB.total_cnt += 1;
                }
            } 
            front_term = end_term;
            front_term_idx = end_term_idx;
            if(strlen(ptr)==0){
                end_term = &db->termDB.nodes[2];
                end_term_idx = 2;
            }
        }
    }
    return 0;
}