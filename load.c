#include "graph.h"
#include "load.h"
#include "query.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int process_line(char *line, char *bopomo, char *words){
    char *buf = strtok(line, ",");
    char *spell_ptr = bopomo;
    char *ptr = NULL;
    while (buf != NULL)
    {   
        char *break_point = strchr(buf, ' ');
        *break_point = 0;
        ptr = buf;
        while(*ptr!=0){
            if(isalnum(*ptr)){
                return 1;
            }
            ptr++;
        }
        strcat(words, buf);
        strcat(words, "_");
        break_point+=1;
        while(*break_point!=0){
            *spell_ptr = *break_point;
            spell_ptr++;
            break_point++;
        }
        *spell_ptr = '_';
        spell_ptr ++;
        buf = strtok(NULL, ",");
    }
    strcat(words, "_");
    *spell_ptr = '_';
    char *p = bopomo;
    // printf("%s\n",words);
    // printf("%s\n",bopomo);
    while(*p!=0){
        if(*p == '_'){
            *p = 0;
        }
        p++;
    }
    p = words;
    while(*p!=0){
        if(*p == '_'){
            *p = 0;
        }
        p++;
    }
    return 0;
}

void db_load_ori(graphDB* db, char *path){
    FILE *fp = fopen(path, "r");
    char line[1000000], bopomo[600000], words[40000];
    // sent insert
    int cnt = 0;
    while (fgets(line, 1000000, fp))
    {
        // if (cnt == 2)
        //     break;
        cnt++;
        if (line[strlen(line) - 1] == '\n'){
            line[strlen(line) - 1] = 0;
        }
        memset(bopomo, 0, sizeof(bopomo));
        memset(words, 0, sizeof(words));
        int invalid = process_line(line, bopomo, words);
        if(!invalid){
            sent_insert(db, words, bopomo);
        }
    }
}