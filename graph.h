#define ID_LENGTH 33
#define SOUND_LENGTH 50
#define WORD_LENGTH 128
#define SOUND_ARRAY_LENGTH 10
#define TERM_ARRAY_LENGTH 1000
#define EDGE_ARRAY_LENGTH 1000
#define SOUND_DB_LENGTH (1<<15)
#define MAX_TERM_CNT (1<<17)
#define MAX_EDGE_CNT (1<<18)
#define int32 unsigned int

typedef struct term_node term_node;
typedef struct sound_node sound_node;
typedef struct full_sound_edge full_sound_edge;
typedef struct prefix_edge prefix_edge;
typedef struct term_edge term_edge;
typedef struct term_edge_db term_edge_db;
typedef struct prefix_edge_db prefix_edge_db;
typedef struct full_sound_edge_db full_sound_edge_db;
typedef struct sound_db sound_db;
typedef struct term_db term_db;
typedef struct graphDB graphDB;

struct term_node{
    // char id[ID_LENGTH];
    // int id;
    char term[WORD_LENGTH];
    int total_freq;
    int last_use;
    int prev_edge[EDGE_ARRAY_LENGTH];
    int next_edge[EDGE_ARRAY_LENGTH];
    int full_sound[SOUND_ARRAY_LENGTH];
};

struct sound_node{
    char sound[SOUND_LENGTH];
};

struct full_sound_edge{
    char id[ID_LENGTH];
    int sound_side[SOUND_LENGTH];
    int term_side[TERM_ARRAY_LENGTH];;
    int freq[TERM_ARRAY_LENGTH];
};

struct prefix_edge{
    char id[ID_LENGTH];
    int term_side[TERM_ARRAY_LENGTH];
    int freq[TERM_ARRAY_LENGTH];
};

struct term_edge{
    char id[ID_LENGTH];
    int last_use;
    int point_from;
    int point_to;
    int freq;
};


struct term_edge_db{
    struct term_edge edges[MAX_EDGE_CNT];
    int total_cnt;
    int hashTab[MAX_EDGE_CNT*2+1];
    int maxHash;
};

struct prefix_edge_db{
    struct prefix_edge edges[MAX_EDGE_CNT];
    int total_cnt;
    int hashTab[MAX_EDGE_CNT*2+1];
    int maxHash;
};

struct full_sound_edge_db{
    struct full_sound_edge edges[MAX_EDGE_CNT];
    int total_cnt;
    int hashTab[MAX_EDGE_CNT*2+1];
    int maxHash;
};

struct sound_db{
    struct sound_node nodes[SOUND_DB_LENGTH];
    int total_cnt;
    int hashTab[SOUND_DB_LENGTH*2+1];
    int maxHash;
};

struct term_db{
    struct term_node nodes[MAX_TERM_CNT];
    int term_cnt;
    int total_cnt;
    int last_term_idx;
    int hashTab[MAX_TERM_CNT*2+1];
    int maxHash;
};

struct graphDB{
    term_db termDB;
    sound_db soundDB;
    full_sound_edge_db full_sound_edgeDB;
    prefix_edge_db prefix_edgeDB;
    term_edge_db term_edgeDB;
};


int get_current_time_stamp();
void md5(char *word, char *hash_code);
unsigned long long hash33(unsigned char *key);
void creat_term_node(term_node *new_node,char *term);
void creat_sound_node(sound_node *new_node,char *sound);
void graph_init(graphDB *db);
void term_db_init(term_db *db);
void sound_db_init(sound_db *db);
void creat_full_sound_edge(full_sound_edge *new_node,char *sound);
void full_sound_edge_db_init(full_sound_edge_db *db);
void creat_prefix_edge(prefix_edge *new_node,char *sound);
void prefix_edge_db_init(prefix_edge_db *db);
void creat_term_edge(term_edge *new_node,char* key1, char* key2,int tid1,int tid2);
void term_edge_db_init(term_edge_db *db);
void prefix_edge_insert(graphDB *db,char *sound,char *term);
void full_sound_insert(graphDB *db,char *sound,char *term);
void term_insert(graphDB *db,char *term);
void sound_insert(graphDB *db,char *sound);
void term_edge_insert(graphDB *db,char *front_term,char *end_term);
int sent_insert(graphDB *db, char *sent, char *sound);