#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>     //for getopt, fork
#include <string.h>     //for strcat
//for struct evkeyvalq
#include <sys/queue.h>
#include <event.h>
//for http
//#include <evhttp.h>
#include <event2/http.h>
#include <event2/http_struct.h>
#include <event2/http_compat.h>
#include <event2/util.h>
#include <signal.h>
#include "../personal_dict/graph.h"
#include "../personal_dict/query.h"
#include "../personal_dict/load.h"
#include "../personal_dict/serialize.h"
#include "../personal_dict/delete.h"
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#define MYHTTPD_SIGNATURE   "myhttpd v 0.0.1"
#define MAX_URL_LEN 10240
#define MAX_FRAG_LEN 10240
//seen_table *db;
void print_prefix_edge(graphDB *db,int idx,char* output);
void print_term_node(graphDB *db,int idx,char* output);
void print_sound_node(graphDB *db,int idx,char* output);
void print_terms_in_sound(graphDB *db,int idx,char* output);
// char* print_term_info(graphDB *db,int idx);
void print_term_edge(graphDB *db,int idx,char* output);
void print_db_state(graphDB *db);
//处理模块
void httpd_handler(struct evhttp_request *req, void *arg) {
    evhttp_add_header(req->output_headers, "Server", MYHTTPD_SIGNATURE);
    evhttp_add_header(req->output_headers, "Content-Type", "text/plain; charset=UTF-8");
    evhttp_add_header(req->output_headers, "Connection", "close");
    //输出的内容
    struct evbuffer *buf;
    buf = evbuffer_new();
    evbuffer_add_printf(buf, "It works!\n");
    evhttp_send_reply(req, HTTP_OK, "OK", buf);
    evbuffer_free(buf);
}

void show_help() {
    char *help = "http://localhost:8080\n"
        "-l <ip_addr> interface to listen on, default is 0.0.0.0\n"
        "-p <num>     port number to listen on, default is 1984\n"
        "-d           run as a deamon\n"
        "-t <second>  timeout for a http request, default is 120 seconds\n"
        "-h           print this help and exit\n"
        "\n";
    fprintf(stderr,"%s",help);
}
//当向进程发出SIGTERM/SIGHUP/SIGINT/SIGQUIT的时候，终止event的事件侦听循环
void signal_handler(int sig) {
    switch (sig) {
        case SIGTERM:
        case SIGHUP:
        case SIGQUIT:
        case SIGINT:
            event_loopbreak();  //终止侦听event_dispatch()的事件侦听循环，执行之后的代码
            printf("server over.");
            break;
            // FILE *fp = fopen ("seen.db", "w");
            // fwrite (db, sizeof(seen_table), 1, fp);
            // break;
    }
}
void print_db_state(graphDB *db){
    printf("db共%d個不同的詞\n",db->termDB.term_cnt);
    printf("db共處理%d個詞\n",db->termDB.total_cnt);
}

void print_prefix_edge(graphDB *db,int idx,char* output){
    if(idx > 0){
        prefix_edge *ptr = &db->prefix_edgeDB.edges[idx];
        for(int i=0;i<TERM_ARRAY_LENGTH;i++){
            if(ptr->term_side[i]!=0){
                print_term_node(db,ptr->term_side[i],output);
                printf("詞頻：%d\n",ptr->freq[i]);
            }
        }
    }
    else{
        strcat(output,"not found");
        printf("not found\n");
    }
}

void print_terms_in_sound(graphDB *db,int idx,char* output){
    printf("terms_in_sound");
    full_sound_edge *ptr = &db->full_sound_edgeDB.edges[idx];
    
    for(int i=0;i<TERM_ARRAY_LENGTH;i++){
        if(ptr->term_side[i]!=0){
            print_term_node(db,ptr->term_side[i],output);
            // strcat(temp_str,ptr->freq[i]);
            printf("詞頻：%d\n",ptr->freq[i]);
        }
    }
    // printf("%s\n",output);
}

void print_term_node(graphDB *db,int idx,char* output){
    term_node *ptr = &db->termDB.nodes[idx];
    strcat(output,ptr->term);
    strcat(output,"\n");

    printf("%s\n",ptr->term);
    printf("freq:%d\n",ptr->total_freq);
    for(int i=0;i<SOUND_ARRAY_LENGTH;i++){
        if(ptr->full_sound[i]!=0){
            int sid = ptr->full_sound[i];
            full_sound_edge *fs_ptr = &db->full_sound_edgeDB.edges[sid];
            for(int j=0;j<SOUND_LENGTH;j++){
                if(fs_ptr->sound_side[j]!=0){
                    print_sound_node(db,fs_ptr->sound_side[j],output);
                    strcat(output," ");
                    printf(" ");
                }
                else{
                    strcat(output,"\n");
                    printf("\n");
                    break;
                }
            }
        }
    }
}

void print_sound_node(graphDB *db,int idx,char* output){
    // printf("%s",db->soundDB.nodes[idx].sound);
    strcat(output,db->soundDB.nodes[idx].sound);
}
/*
char* print_term_info(graphDB *db,int idx){
    if(idx>0){
        term_node *ptr = &db->termDB.nodes[idx];
        char temp_str[10000];
        strcpy(temp_str,ptr->term);
        strcat(temp_str,ptr->term);
        strcat(temp_str,'\nfreq:');
        strcat(temp_str,ptr->total_freq);
        strcat(temp_str,'\nfreq:');
        strcat(temp_str,'\nfreq:');
        strcat(temp_str,'\nfreq:');
        printf("%s\n",ptr->term);
        printf("freq:%d\n",ptr->total_freq);
        printf("last use:%d\n",ptr->last_use);
        printf("sound:\n");
        for(int i=0;i<SOUND_ARRAY_LENGTH;i++){
            if(ptr->full_sound[i]!=0){
                int sid = ptr->full_sound[i];
                int flag = 0;
                full_sound_edge *fs_ptr = &db->full_sound_edgeDB.edges[sid];
                for(int j=0;j<SOUND_LENGTH;j++){
                    if(fs_ptr->sound_side[j]!=0){
                        flag = 1;
                        print_sound_node(db,fs_ptr->sound_side[j]);
                        printf(" ");
                    }
                    else{
                        if(flag){
                            printf("(%d)\n",fs_ptr->freq[i]);
                        }
                        break;
                    }
                }
            }
        }
        printf("prev edge:\n");
        for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
            if(ptr->prev_edge[i]>0){
                int edge_idx = ptr->prev_edge[i];
                term_edge *qtr = &db->term_edgeDB.edges[edge_idx];
                int idx2 = qtr->point_from;
                printf("%s(id:%d)--->%s(id:%d)(last_use:%d)\n",db->termDB.nodes[idx2].term,qtr->point_from,db->termDB.nodes[qtr->point_to].term,qtr->point_to,qtr->last_use);
            }
        }
        printf("next edge:\n");
        for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
            if(ptr->next_edge[i]>0){
                int edge_idx = ptr->next_edge[i];
                term_edge *qtr = &db->term_edgeDB.edges[edge_idx];
                int idx2 = qtr->point_to;
                printf("%s(id:%d)--->%s(id:%d)(last_use:%d)\n",db->termDB.nodes[qtr->point_from].term,qtr->point_from,db->termDB.nodes[idx2].term,qtr->point_to,qtr->last_use);
            }
        }
    }
    else{
        printf("not found\n");
    }
}
*/
void print_term_edge(graphDB *db,int idx,char* output){
    if(idx == 0){
        strcat(output,"not found\n");
        printf("not found\n");
    }else{
        term_edge *ptr = &db->term_edgeDB.edges[idx];
        strcat(output,"not found\n");
        strcat(output,db->termDB.nodes[ptr->point_from].term);
        strcat(output,"---->");
        strcat(output,db->termDB.nodes[ptr->point_to].term);
        strcat(output,"\n");
        
        printf("%s--->%s\n",db->termDB.nodes[ptr->point_from].term,db->termDB.nodes[ptr->point_to].term);
        printf("frequency:%d\n",ptr->freq);
        printf("last use:%d\n",ptr->last_use);
    }
}

/*
void check_list(graphDB *db,char *urls,char *output){
    char *ptr = urls;
    char *qtr = output;
    char current_url[MAX_URL_LEN];
    char fragment[MAX_FRAG_LEN];
    char *current_ptr = current_url;
    char *frag_ptr = fragment;
    int useless = 0;
    memset(current_url, '\0',sizeof(char)*MAX_URL_LEN);
    memset(fragment, '\0',sizeof(char)*MAX_FRAG_LEN);
    while (*ptr!='\0'){
        if (*ptr!='\n'){
            if (*ptr == '#'){
                useless = 1;
            }
            if (!useless){
                *current_ptr++ = *ptr++;
            }
            else{
                *frag_ptr++ = *ptr++;
            }
        }
        else{
            useless = 0;
            *current_ptr = '\0';
            *frag_ptr = '\0';
            //check if existed
            printf("checking:%s\n",current_url);
            unsigned char decrypt[17];
            unsigned char key[5];
            memset(key,0,sizeof(unsigned char)*5);
            memset(decrypt,0,sizeof(unsigned char)*17);
            get_url_md5(current_url,decrypt);
            unsigned int addr = get_url_addr(decrypt);
            get_url_identity(decrypt,key);
            unsigned int is_existed = check_url_in_table(db,addr,key);
            if(is_existed == db->table_idx[addr]){
                printf("not found\n");
                insert_url_to_table(db,addr,key);
                printf("insert finish\n");
                strcat(output,current_url);
                strcat(output,fragment);
                strcat(output,"\n");
            }         
            current_ptr = current_url;
            frag_ptr = fragment;
            memset(current_url,'\0',sizeof(char)*MAX_URL_LEN);
            memset(fragment, '\0',sizeof(char)*MAX_FRAG_LEN);
            ptr++;
        }
    }
    printf("finish all urls\n");
    if(output[strlen(output)-1] == '\n'){
        output[strlen(output)-1] = '\0';
    }
}

*/
void check_handler(struct evhttp_request *req, void *argv) {
    graphDB *db = (graphDB *)argv;
    struct evbuffer* buf = NULL;
    size_t len = 0;
    char* data = NULL;
    // get the event buffer containing POST body
    buf = evhttp_request_get_input_buffer(req);
    // get the length of POST body
    len = evbuffer_get_length(buf);
    // create a char array to extract POST body
    data = malloc(len + 1);
    char *output = (char*)malloc(sizeof(char)*(len + 10000));
    memset(output,'\0',sizeof(char)*(len + 1));
    memset(data,'\0',sizeof(char)*(len + 1));
    // copy POST body into your char array
    evbuffer_copyout(buf, data, len);
    printf(data);
    char * token = strtok(data, ",");
    int token_len = strlen(token) + 1;
    char method[token_len];
    strcpy(method,token);
    printf("%s\n", method);
    token = strtok(NULL, ",");
    token_len = strlen(token) + 1;
    char para1[token_len];
    strcpy(para1,token);
    printf("%s\n", para1);
    token = strtok(NULL, ",");
    token_len = strlen(token) + 1;
    char para2[token_len];
    strcpy(para2,token);
    printf("%s\n", para2);
    char line[1000000], bopomo[600000], words[40000];
    if(strcmp(method,"query_term_edge") == 0){
        int rsp = query_term_edge(db,para1,para2);
        printf("-----\n");
        print_term_edge(db,rsp,output);
    } else if(strcmp(method,"query_prefix") == 0){
        int rsp = query_prefix(db,para1);
        printf("-----\n");
        print_prefix_edge(db,rsp,output);
    } else if(strcmp(method,"query_terms_by_sound") == 0){
        int rsp = query_terms_by_sound(db,para1);
        if(rsp>0){
            print_terms_in_sound(db,rsp,output);
        }else{
            strcpy(output,"not found");
            printf("not found\n");
        }
    } else if(strcmp(method,"delete_sound_in_term") == 0){
        int rsp = query_terms_by_sound(db,para1);
        printf("-----\n");
        if(rsp>0){
            print_terms_in_sound(db,rsp,output);
        }else{
            strcpy(output,"not found");
            printf("not found\n");
        }
    } else if(strcmp(method,"sent_insert") == 0){
        memset(bopomo, 0, sizeof(bopomo));
        memset(words, 0, sizeof(words));
        int invalid = process_line(para1, bopomo, words);
        if(!invalid){
            sent_insert(db, words, bopomo);
        }
    }
    
    free(data);
    //HTTP header
    evhttp_add_header(req->output_headers, "Server", MYHTTPD_SIGNATURE);
    evhttp_add_header(req->output_headers, "Content-Type", "text/plain; charset=UTF-8");
    evhttp_add_header(req->output_headers, "Connection", "close");
    //输出的内容
    struct evbuffer *rsp_buf = evbuffer_new();
    evbuffer_add_printf(rsp_buf, "%s",output);
    evhttp_send_reply(req, HTTP_OK, "OK", rsp_buf);
    evbuffer_free(rsp_buf);
}
int main(int argc, char *argv[]) {
    char prefix_key[33];
    char corpus[128];
    char dict[128];
    graphDB *db = NULL;
    memset(corpus,'\0',sizeof(char)*128);
    memset(dict,'\0',sizeof(char)*128);
    for(int i=1;i<argc;i++){
        if(strcmp(argv[i],"--corpus\0") == 0){
            strcpy(corpus,argv[i+1]);
        }
        else if(strcmp(argv[i],"--dict\0") == 0){
            strcpy(dict,argv[i+1]);
        }
    }
    if(strlen(dict) == 0){
        strcpy(dict,"person.dict\0");
    }
    if(strlen(corpus) == 0){
        strcpy(corpus,"./wiki.tw.seg.bopomo.small.txt\0");
    }
    int fd = open(dict, O_RDWR | O_CREAT , S_IRWXU);
    if (fd == -1) {
        printf("Failed to create version vector file, error is '%s'", strerror(errno));
        return 1;
    }
    struct stat st;
    fstat(fd, &st);
    if (st.st_size ==  0) {
        ftruncate(fd, sizeof(graphDB));
        db = (graphDB *) mmap(0, sizeof(graphDB), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        graph_init(db);
        //load data
        db_load_ori(db,corpus);
        msync(db, sizeof(graphDB), MS_SYNC);
    }else{
        db = (graphDB *) mmap(0, st.st_size, PROT_READ| PROT_WRITE, MAP_SHARED, fd, 0);
        if( db == MAP_FAILED){
            printf("mmap failed with error '%s'\n", strerror(errno));
            exit(0);
        }
    }

    signal(SIGHUP, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGQUIT, signal_handler);

    //默认参数
    char *httpd_option_listen = "0.0.0.0";
    int httpd_option_port = 8080;
    int httpd_option_daemon = 0;
    int httpd_option_timeout = 120; //in seconds
    //获取参数
    int c;
    while ((c = getopt(argc, argv, "l:p:dt:h")) != -1) {
        switch (c) {
            case 'l' :
                httpd_option_listen = atoi(optarg);
                break;
            case 'p' :
                httpd_option_port = atoi(optarg);
                break;
            case 'd' :
                httpd_option_daemon = 1;
                break;
            case 't' :
                httpd_option_timeout = atoi(optarg);
                break;
            case 'h' :
            default :
                show_help();
                exit(EXIT_SUCCESS);
        }
    }

    //判断是否设置了-d，以daemon运行
    if (httpd_option_daemon) {
        pid_t pid;
        pid = fork();
        if (pid < 0) {
            perror("fork failed");
            exit(EXIT_FAILURE);
        }
        if (pid > 0) {
            //生成子进程成功，退出父进程
            exit(EXIT_SUCCESS);
        }
    }

    /* 使用libevent创建HTTP Server */

    //初始化event API
    event_init();

    //创建一个http server
    struct evhttp *httpd;
    httpd = evhttp_start(httpd_option_listen, httpd_option_port);
    evhttp_set_timeout(httpd, httpd_option_timeout);

    //指定generic callback
    evhttp_set_gencb(httpd, httpd_handler, NULL);
    //也可以为特定的URI指定callback
    evhttp_set_cb(httpd, "/term_db",check_handler, db);
    //循环处理events
    event_dispatch();

    evhttp_free(httpd);
    return 0;
}
