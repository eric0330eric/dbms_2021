void delete_term(graphDB *db,char *term);
void delete_edge(graphDB *db,char *term1,char *term2);
void delete_sound_in_term(graphDB *db,char *term,char *sound);
void delete_prefix_in_term(graphDB *db,char *term,char *sound);