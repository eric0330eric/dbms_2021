#include "graph.h"
#include "query.h"
#include "load.h"
#include "serialize.h"
#include "delete.h"
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

void print_prefix_edge(graphDB *db,int idx);
void print_term_node(graphDB *db,int idx);
void print_sound_node(graphDB *db,int idx);
void print_terms_in_sound(graphDB *db,int idx);
void print_term_info(graphDB *db,int idx);
void print_term_edge(graphDB *db,int idx);
void print_db_state(graphDB *db);

int main(int argc, char *argv[])
{
    char prefix_key[33];
    char corpus[128];
    char dict[128];
    graphDB *db = NULL;
    memset(corpus,'\0',sizeof(char)*128);
    memset(dict,'\0',sizeof(char)*128);
    for(int i=1;i<argc;i++){
        if(strcmp(argv[i],"--corpus\0") == 0){
            strcpy(corpus,argv[i+1]);
        }
        else if(strcmp(argv[i],"--dict\0") == 0){
            strcpy(dict,argv[i+1]);
        }
    }
    if(strlen(dict) == 0){
        strcpy(dict,"person.dict\0");
    }
    if(strlen(corpus) == 0){
        strcpy(corpus,"./wiki.tw.seg.bopomo.small.txt\0");
    }
    int fd = open(dict, O_RDWR | O_CREAT , S_IRWXU);
    if (fd == -1) {
        printf("Failed to create version vector file, error is '%s'", strerror(errno));
        return 1;
    }
    struct stat st;
    fstat(fd, &st);
    if (st.st_size ==  0) {
        ftruncate(fd, sizeof(graphDB));
        db = (graphDB *) mmap(0, sizeof(graphDB), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        graph_init(db);
        //load data
        db_load_ori(db,corpus);
        msync(db, sizeof(graphDB), MS_SYNC);
    }else{
        db = (graphDB *) mmap(0, st.st_size, PROT_READ| PROT_WRITE, MAP_SHARED, fd, 0);
        if( db == MAP_FAILED){
            printf("mmap failed with error '%s'\n", strerror(errno));
            exit(0);
        }
    }
    int rsp = query_term_edge(db,"的\0","朋友\0");
    printf("-----\n");
    print_term_edge(db,rsp);
    rsp = query_prefix(db,"ㄔㄥ2\0");
    printf("-----\n");
    print_prefix_edge(db,rsp);
    rsp = query_terms_by_sound(db,"ㄔㄥ2ㄕ4\0");
    printf("-----\n");
    if(rsp>0){
        print_terms_in_sound(db,rsp);
    }else{
        printf("not found\n");
    }
    delete_sound_in_term(db,"乘勢\0","ㄔㄥ2ㄕ4\0");
    rsp = query_terms_by_sound(db,"ㄔㄥ2ㄕ4\0");
    printf("-----\n");
    if(rsp>0){
        print_terms_in_sound(db,rsp);
    }else{
        printf("not found\n");
    }
    return 0;
}

void print_db_state(graphDB *db){
    printf("db共%d個不同的詞\n",db->termDB.term_cnt);
    printf("db共處理%d個詞\n",db->termDB.total_cnt);
}

void print_prefix_edge(graphDB *db,int idx){
    if(idx > 0){
        prefix_edge *ptr = &db->prefix_edgeDB.edges[idx];
        for(int i=0;i<TERM_ARRAY_LENGTH;i++){
            if(ptr->term_side[i]!=0){
                print_term_node(db,ptr->term_side[i]);
                printf("詞頻：%d\n",ptr->freq[i]);
            }
        }
    }
    else{
        printf("not found\n");
    }

}

void print_terms_in_sound(graphDB *db,int idx){
    full_sound_edge *ptr = &db->full_sound_edgeDB.edges[idx];
    for(int i=0;i<TERM_ARRAY_LENGTH;i++){
        if(ptr->term_side[i]!=0){
            print_term_node(db,ptr->term_side[i]);
            printf("詞頻：%d\n",ptr->freq[i]);
        }
    }
}

void print_term_node(graphDB *db,int idx){
    term_node *ptr = &db->termDB.nodes[idx];
    printf("%s\n",ptr->term);
    printf("freq:%d\n",ptr->total_freq);
    for(int i=0;i<SOUND_ARRAY_LENGTH;i++){
        if(ptr->full_sound[i]!=0){
            int sid = ptr->full_sound[i];
            full_sound_edge *fs_ptr = &db->full_sound_edgeDB.edges[sid];
            for(int j=0;j<SOUND_LENGTH;j++){
                if(fs_ptr->sound_side[j]!=0){
                    print_sound_node(db,fs_ptr->sound_side[j]);
                    printf(" ");
                }
                else{
                    printf("\n");
                    break;
                }
            }
        }
    }
}

void print_sound_node(graphDB *db,int idx){
    printf("%s",db->soundDB.nodes[idx].sound);
}

void print_term_info(graphDB *db,int idx){
    if(idx>0){
        term_node *ptr = &db->termDB.nodes[idx];
        printf("%s\n",ptr->term);
        printf("freq:%d\n",ptr->total_freq);
        printf("last use:%d\n",ptr->last_use);
        printf("sound:\n");
        for(int i=0;i<SOUND_ARRAY_LENGTH;i++){
            if(ptr->full_sound[i]!=0){
                int sid = ptr->full_sound[i];
                int flag = 0;
                full_sound_edge *fs_ptr = &db->full_sound_edgeDB.edges[sid];
                for(int j=0;j<SOUND_LENGTH;j++){
                    if(fs_ptr->sound_side[j]!=0){
                        flag = 1;
                        print_sound_node(db,fs_ptr->sound_side[j]);
                        printf(" ");
                    }
                    else{
                        if(flag){
                            printf("(%d)\n",fs_ptr->freq[i]);
                        }
                        break;
                    }
                }
            }
        }
        printf("prev edge:\n");
        for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
            if(ptr->prev_edge[i]>0){
                int edge_idx = ptr->prev_edge[i];
                term_edge *qtr = &db->term_edgeDB.edges[edge_idx];
                int idx2 = qtr->point_from;
                printf("%s(id:%d)--->%s(id:%d)(last_use:%d)\n",db->termDB.nodes[idx2].term,qtr->point_from,db->termDB.nodes[qtr->point_to].term,qtr->point_to,qtr->last_use);
            }
        }
        printf("next edge:\n");
        for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
            if(ptr->next_edge[i]>0){
                int edge_idx = ptr->next_edge[i];
                term_edge *qtr = &db->term_edgeDB.edges[edge_idx];
                int idx2 = qtr->point_to;
                printf("%s(id:%d)--->%s(id:%d)(last_use:%d)\n",db->termDB.nodes[qtr->point_from].term,qtr->point_from,db->termDB.nodes[idx2].term,qtr->point_to,qtr->last_use);
            }
        }
    }
    else{
        printf("not found\n");
    }
}

void print_term_edge(graphDB *db,int idx){
    if(idx == 0){
        printf("not found\n");
    }else{
        term_edge *ptr = &db->term_edgeDB.edges[idx];
        printf("%s--->%s\n",db->termDB.nodes[ptr->point_from].term,db->termDB.nodes[ptr->point_to].term);
        printf("frequency:%d\n",ptr->freq);
        printf("last use:%d\n",ptr->last_use);
    }
}