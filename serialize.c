#include "graph.h"
#include "serialize.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void dict_save(graphDB *db,char *filename){
    FILE * file= fopen(filename, "wb");
    if (file != NULL) {
        fwrite(db, sizeof(graphDB), 1, file);
        fclose(file);
    }
}

void dict_load(graphDB *db,char *filename){
    FILE * file= fopen(filename, "rb");
    if (file != NULL) {
        fread(db, sizeof(graphDB), 1, file);
        fclose(file);
    }
}