int query_prefix(graphDB *db,char *prefix);
int query_terms_by_sound(graphDB *db,char *sound);
int query_term(graphDB *db,char *term);
int query_term_edge(graphDB *db,char *term1,char *term2);