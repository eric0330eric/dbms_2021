#include "graph.h"
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

// msync(db, sizeof(graphDB), MS_SYNC);

int get_prefix(graphDB *db,full_sound_edge *edge){
	char sound[1024];
	char key[33];
	memset(sound,'\0',sizeof(char)*1024);
	memset(key,'\0',sizeof(char)*33);
	int term_len = 0;
	for(int i = 0;i < SOUND_LENGTH; i++){
		int idx = edge->sound_side[i];
		if(idx!=0){
			term_len += 1;
		}else{
			break;
		}
	}
	term_len = (term_len+1)/2;
	for(int i = 0;i < term_len; i++){
		int idx = edge->sound_side[i];
		if(idx!=0){
			strcat(sound,db->soundDB.nodes[idx].sound);
		}else{
			break;
		}
	}
	md5(sound,key);
	int hv = hash33(key);
	hv = hv % db->prefix_edgeDB.maxHash;
	int tid = db->prefix_edgeDB.hashTab[hv];
	while(hv!=0){
		if(tid<0){
			hv = (hv+1) % db->prefix_edgeDB.maxHash;
			tid = db->prefix_edgeDB.hashTab[hv];
			continue;
		}
		if(strcmp(db->prefix_edgeDB.edges[tid].id,key) == 0){
			return hv;
			break;
		}
		hv = (hv+1) % db->prefix_edgeDB.maxHash;
		tid = db->prefix_edgeDB.hashTab[hv];
	}
	return hv;
}

void delete_term(graphDB *db,char *term){
	char key[33];
	int hv = 0,tid = 0;
	md5(term,key);
	hv = hash33(key);
	hv = hv % db->termDB.maxHash;
	tid = db->termDB.hashTab[hv];
	while(tid != 0){
		if(tid<0){
			hv = (hv+1) % db->termDB.maxHash;
			tid = db->termDB.hashTab[hv];
			continue;
		}
		if(!strcmp(db->termDB.nodes[tid].term,term)){
			db->termDB.hashTab[hv] = -1 *  db->termDB.hashTab[hv];
			memset(db->termDB.nodes[tid].term,'\0',sizeof(char)*WORD_LENGTH);
			db->termDB.nodes[tid].last_use = 0;
			db->termDB.term_cnt=db->termDB.term_cnt - 1;
			db->termDB.total_cnt=db->termDB.total_cnt-db->termDB.nodes[tid].total_freq;
			db->termDB.nodes[tid].total_freq = 0;
			// 處理指向node的term edge
			for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
				int idx = db->termDB.nodes[tid].prev_edge[i];
				term_edge *ptr = &db->term_edgeDB.edges[idx];
				int edge_hv = hash33(ptr->id);
				edge_hv = edge_hv % db->term_edgeDB.maxHash;
				int edge_tid = db->term_edgeDB.hashTab[edge_hv];
				while(edge_tid!=0){
					if(edge_tid<0){
						edge_hv = (edge_hv+1) % db->term_edgeDB.maxHash;
						edge_tid = db->term_edgeDB.hashTab[edge_hv];
						continue;
					}
					if(strcmp(db->term_edgeDB.edges[edge_tid].id,ptr->id) == 0){
						db->term_edgeDB.hashTab[edge_hv] = -1 * db->term_edgeDB.hashTab[edge_hv];
						break;
					}
					edge_hv = (edge_hv+1) % db->term_edgeDB.maxHash;
					edge_tid = db->term_edgeDB.hashTab[edge_hv];
				}
				for(int j=0;j<EDGE_ARRAY_LENGTH;j++){
					if(db->termDB.nodes[ptr->point_from].next_edge[j] == idx){
						db->termDB.nodes[ptr->point_from].next_edge[j] = 0;
						break;
					}
				}
				memset(ptr->id,'\0',sizeof(char)*ID_LENGTH);
				ptr->last_use = 0;
				ptr->point_from = 0;
				ptr->point_to = 0;
				ptr->freq = 0;
			}
			// 處理從node指出去的term edge
			for(int i=0;i<EDGE_ARRAY_LENGTH;i++){
				int idx = db->termDB.nodes[tid].next_edge[i];
				term_edge *ptr = &db->term_edgeDB.edges[idx];
				int edge_hv = hash33(ptr->id);
				edge_hv = edge_hv % db->term_edgeDB.maxHash;
				int edge_tid = db->term_edgeDB.hashTab[edge_hv];
				while(edge_tid!=0){
					if(edge_tid<0){
						edge_hv = (edge_hv+1) % db->term_edgeDB.maxHash;
						edge_tid = db->term_edgeDB.hashTab[edge_hv];
						continue;
					}
					if(strcmp(db->term_edgeDB.edges[edge_tid].id,ptr->id) == 0){
						db->term_edgeDB.hashTab[edge_hv] = -1 * db->term_edgeDB.hashTab[edge_hv];
						break;
					}
					edge_hv = (edge_hv+1) % db->term_edgeDB.maxHash;
					edge_tid = db->term_edgeDB.hashTab[edge_hv];
				}
				for(int j=0;j<EDGE_ARRAY_LENGTH;j++){
					if(db->termDB.nodes[ptr->point_to].prev_edge[j] == idx){
						db->termDB.nodes[ptr->point_to].prev_edge[j] = 0;
						break;
					}
				}
				memset(ptr->id,'\0',sizeof(char)*ID_LENGTH);
				ptr->last_use = 0;
				ptr->point_from = 0;
				ptr->point_to = 0;
				ptr->freq = 0;
			}
			// 處理全音edge
			for(int i=0;i<SOUND_ARRAY_LENGTH;i++){
				int idx = db->termDB.nodes[tid].full_sound[i];
				if(idx){
					full_sound_edge *ptr = &db->full_sound_edgeDB.edges[idx];
					//處理prefix edge
					int prefix_hv = get_prefix(db,ptr);
					int prefix_tid = db->prefix_edgeDB.hashTab[prefix_hv];
					if(prefix_tid!= 0){
						prefix_edge *prefix_ptr = &db->prefix_edgeDB.edges[prefix_tid];
						int flag = 0;
						for(int j=0;j<TERM_ARRAY_LENGTH;j++){
							if(prefix_ptr->term_side[j] == tid){
								prefix_ptr->term_side[j] = 0;
								prefix_ptr->freq[j] = 0;
							}
							else if(prefix_ptr->term_side[j]!=0){
								flag = 1;
							}
						}
						if(!flag){
							memset(prefix_ptr->id,'\0',sizeof(char)*ID_LENGTH);
							db->prefix_edgeDB.hashTab[prefix_hv] = -1 * db->prefix_edgeDB.hashTab[prefix_hv];
						}
					}
					// 處理 full sound
					int flag = 0;
					for(int j=0;j<TERM_ARRAY_LENGTH;j++){
						if(ptr->term_side[j] == tid){
							ptr->term_side[j] = 0;
							ptr->freq[j] = 0;
						}
						else if(ptr->term_side[j]!=0){
							flag = 1;
						}
					}
					if(!flag){
						int sound_hv = hash33(ptr->id);
						sound_hv = sound_hv % db->full_sound_edgeDB.maxHash;
						int sound_tid = db->full_sound_edgeDB.hashTab[sound_hv];
						while(sound_tid!=0){
							if(sound_tid<0){
								sound_hv = (sound_hv+1) % db->full_sound_edgeDB.maxHash;
								sound_tid = db->full_sound_edgeDB.hashTab[sound_hv];
								continue;
							}
							if(strcmp(db->full_sound_edgeDB.edges[sound_tid].id,ptr->id) == 0){
								db->full_sound_edgeDB.hashTab[sound_hv] = -1 * db->full_sound_edgeDB.hashTab[sound_hv];
								break;
							}
							sound_hv = (sound_hv+1) % db->full_sound_edgeDB.maxHash;
							sound_tid = db->full_sound_edgeDB.hashTab[sound_hv];
						}
						memset(ptr->id,'\0',sizeof(char)*33);
						for(int j=0;j<SOUND_LENGTH;j++){
							db->full_sound_edgeDB.edges[sound_tid].sound_side[j] = 0;
						}
					}
					db->termDB.nodes[tid].full_sound[i] = 0;
				}
			}
			break;
		}
		hv = (hv+1) % db->termDB.maxHash;
		tid = db->termDB.hashTab[hv];
	}
	msync(db, sizeof(graphDB), MS_SYNC);
}

void delete_edge(graphDB *db,char *term1,char *term2){
	char key1[33],key2[33],concat_key[65],edge_key[33];
	memset(key1,'\0',sizeof(char)*33);
	memset(key2,'\0',sizeof(char)*33);
	memset(concat_key,'\0',sizeof(char)*65);
	memset(edge_key,'\0',sizeof(char)*33);
	md5(term1,key1);
	md5(term2,key2);
	strcat(concat_key,key1);
	strcat(concat_key,key2);
	md5(concat_key,edge_key);
	int hv = hash33(edge_key);
	hv = hv % db->term_edgeDB.maxHash;
	int tid = db->term_edgeDB.hashTab[hv];
	while(tid!=0){
		if(tid<0){
			hv = (hv+1) % db->term_edgeDB.maxHash;
			tid = db->term_edgeDB.hashTab[hv];
			continue;
		}
		if(strcmp(db->term_edgeDB.edges[tid].id,edge_key) == 0){
			term_edge *ptr = &db->term_edgeDB.edges[tid];
			for(int j=0;j<EDGE_ARRAY_LENGTH;j++){
				if(db->termDB.nodes[ptr->point_to].prev_edge[j] == tid){
					db->termDB.nodes[ptr->point_to].prev_edge[j] = 0;
					break;
				}
			}
			for(int j=0;j<EDGE_ARRAY_LENGTH;j++){
				if(db->termDB.nodes[ptr->point_from].next_edge[j] == tid){
					db->termDB.nodes[ptr->point_from].next_edge[j] = 0;
					break;
				}
			}
			memset(ptr->id,'\0',sizeof(char)*ID_LENGTH);
			ptr->last_use = 0;
			ptr->point_from = 0;
			ptr->point_to = 0;
			ptr->freq = 0;
			db->term_edgeDB.hashTab[hv] = -1 * db->term_edgeDB.hashTab[hv];
			break;
		}
		hv = (hv+1) % db->term_edgeDB.maxHash;
		tid = db->term_edgeDB.hashTab[hv];
	}
}

void delete_sound_in_term(graphDB *db,char *term,char *sound){
	char term_key[33],sound_key[33];
	memset(term_key,'\0',sizeof(char)*33);
	memset(sound_key,'\0',sizeof(char)*33);
	md5(term,term_key);
	md5(sound,sound_key);
	int sound_hv = hash33(sound_key);
	sound_hv = sound_hv%db->full_sound_edgeDB.maxHash;
	int sound_tid = db->full_sound_edgeDB.hashTab[sound_hv];
	int term_cnt = 0;
	while(sound_tid!=0){
		if(sound_tid>0){
			if(!strcmp(db->full_sound_edgeDB.edges[sound_tid].id,sound_key)){
				for(int i=0;i<TERM_ARRAY_LENGTH;i++){
					if(db->full_sound_edgeDB.edges[sound_tid].term_side[i] != 0){
						term_cnt += 1;
					}
				}
				break;
			}
		}
		sound_hv = (sound_hv+1) % db->full_sound_edgeDB.maxHash;
		sound_tid = db->full_sound_edgeDB.hashTab[sound_hv];
	}
	int term_hv = hash33(term_key);
	term_hv = term_hv%db->termDB.maxHash;
	int term_tid = db->termDB.hashTab[term_hv];
	int sound_cnt = 0;
	while(term_tid!=0){
		if(term_tid>0){
			if(!strcmp(db->termDB.nodes[term_tid].term,term)){
				int target_sound = -1;
				for(int i=0;i<SOUND_ARRAY_LENGTH;i++){
					if(db->termDB.nodes[term_tid].full_sound[i]!=0){
						sound_cnt++;
						if(db->termDB.nodes[term_tid].full_sound[i] == sound_tid){
							target_sound = i;
						}
					}
				}
				if(sound_cnt>1){
					db->termDB.nodes[term_tid].full_sound[target_sound] = 0;
				}
				break;
			}
		}
		term_hv = (term_hv+1) % db->termDB.maxHash;
		term_tid = db->termDB.hashTab[term_hv];
	}
	if(sound_cnt == 1){
		delete_term(db,term);
	}
	else{
		full_sound_edge *ptr = &db->full_sound_edgeDB.edges[sound_tid];
		//處理prefix edge
		int prefix_hv = get_prefix(db,ptr);
		int prefix_tid = db->prefix_edgeDB.hashTab[prefix_hv];
		if(prefix_tid!= 0){
			prefix_edge *prefix_ptr = &db->prefix_edgeDB.edges[prefix_tid];
			int flag = 0;
			for(int j=0;j<TERM_ARRAY_LENGTH;j++){
				if(prefix_ptr->term_side[j] == term_tid){
					prefix_ptr->term_side[j] = 0;
					prefix_ptr->freq[j] = 0;
				}
				else if(prefix_ptr->term_side[j]!=0){
					flag = 1;
				}
			}
			if(!flag){
				memset(prefix_ptr->id,'\0',sizeof(char)*ID_LENGTH);
				db->prefix_edgeDB.hashTab[prefix_hv] = -1 * db->prefix_edgeDB.hashTab[prefix_hv];
			}
		}
		// 處理full sound
		if(term_cnt > 1){
			for(int j=0;j<TERM_ARRAY_LENGTH;j++){
				if(ptr->term_side[j] == term_tid){
					ptr->term_side[j] = 0;
					ptr->freq[j] = 0;
				}
			}
		}
		else{
			db->full_sound_edgeDB.hashTab[sound_hv] = -1 * db->full_sound_edgeDB.hashTab[sound_hv];
			memset(ptr->id,'\0',sizeof(char)*33);
			for(int j=0;j<SOUND_LENGTH;j++){
				db->full_sound_edgeDB.edges[sound_tid].sound_side[j] = 0;
			}
		}
	}
}

void delete_prefix_in_term(graphDB *db,char *term,char *sound){
	char key[33],term_key[33];
	memset(key,'\0',sizeof(char)*33);
	memset(term_key,'\0',sizeof(char)*33);
	md5(sound,key);
	md5(term,term_key);
	int term_hv = hash33(term_key);
	term_hv = term_hv%db->termDB.maxHash;
	int term_tid = db->termDB.hashTab[term_hv];
	while(term_tid!=0){
		if(term_tid>0){
			if(!strcmp(db->termDB.nodes[term_tid].term,term)){
				break;
			}
		}
		term_hv = (term_hv+1) % db->termDB.maxHash;
		term_tid = db->termDB.hashTab[term_hv];
	}
	int prefix_hv = hash33(key);
	prefix_hv = prefix_hv % db->prefix_edgeDB.maxHash;
	int prefix_tid = db->prefix_edgeDB.hashTab[prefix_hv];
	while(prefix_hv!=0){
		if(prefix_tid<0){
			prefix_hv = (prefix_hv+1) % db->prefix_edgeDB.maxHash;
			prefix_tid = db->prefix_edgeDB.hashTab[prefix_hv];
			continue;
		}
		if(strcmp(db->prefix_edgeDB.edges[prefix_tid].id,key) == 0){
			break;
		}
		prefix_hv = (prefix_hv+1) % db->prefix_edgeDB.maxHash;
		prefix_tid = db->prefix_edgeDB.hashTab[prefix_hv];
	}
	if(prefix_tid!= 0){
		prefix_edge *prefix_ptr = &db->prefix_edgeDB.edges[prefix_tid];
		int flag = 0;
		for(int j=0;j<TERM_ARRAY_LENGTH;j++){
			if(prefix_ptr->term_side[j] == term_tid){
				prefix_ptr->term_side[j] = 0;
				prefix_ptr->freq[j] = 0;
			}
			else if(prefix_ptr->term_side[j]!=0){
				flag = 1;
			}
		}
		if(!flag){
			memset(prefix_ptr->id,'\0',sizeof(char)*ID_LENGTH);
			db->prefix_edgeDB.hashTab[prefix_hv] = -1 * db->prefix_edgeDB.hashTab[prefix_hv];
		}
	}
}
